extends Timer

export(String) var scene_to_load

var timer

func _ready():

    timer = get_node("Timer")

    timer.set_wait_time(60)
    timer.connect("timeout", self, "_on_Timer_timeout")
    timer.start()

func _on_Timer_timeout():
    get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))