extends Area2D

onready var flagPacked = preload("res://Scenes/Dead_Body.tscn")

var newPos

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Spikes_body_entered(body):
	if body.get_name() == "Player":
		get_tree().reload_current_scene()
		var dead_Body = flagPacked.instance()
		if self.rotation_degrees == 0:
			var newPos = Vector2(self.position.x, (self.position.y + -16))
			dead_Body.position = newPos
			get_tree().get_root().add_child(dead_Body)
		else:
			var newPos = Vector2(self.position.x, (self.position.y + 16))
			dead_Body.position = newPos
			get_tree().get_root().add_child(dead_Body)
			