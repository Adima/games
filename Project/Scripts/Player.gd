extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -300

const UP = Vector2(0,-1)

var velocity = Vector2()
var double_jump = true

onready var sprite = self.get_node("sprite")

func get_input():
	if is_on_floor():
		double_jump = true
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
	if is_on_floor() == false and Input.is_action_just_pressed('ui_up') and double_jump == true:
		velocity.y = jump_speed
		double_jump = false
	if is_on_floor() == false and Input.is_action_just_pressed("ui_down"):
		velocity.y = -jump_speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y > 0:
		sprite.play("Jump")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	elif velocity.y < 0:
		sprite.play("Landing")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	elif velocity.x != 0:
		sprite.play("Run")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		sprite.play("Idle")

func _death():
	pass