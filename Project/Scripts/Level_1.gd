extends Node2D

onready var timer = get_node("TimerTimeout")

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.set_wait_time(60)
	timer.start()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TimerTimeout_timeout():
	get_tree().change_scene(str("res://Scenes/Game_Over.tscn"))


func _on_Limit_body_entered(body):
	pass # Replace with function body.
