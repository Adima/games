# Welcome to my little project!
Game Development | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Ganjil 2019/2010

**Dibuat oleh: Aditya Pratama**

***

# The Long Sleep
This is a simple platformer game. What really inspires me to make the concept is the state of lucid dreaming. I've felt it once in a while and it's always a bit scary. How you feel like you are in control but you're really just following what your brain desires. I'm trying to combine a simple platformer with a bit of mystery on the side.

## The Controls
The controls are really simple, just UP, LEFT, and RIGHT. The rest is self explanatory.